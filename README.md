# 基于SpringBoot+Vue实现的社团管理系统

### 系统介绍

基于SpringBoot+Vue实现的社团管理系统是为高校学生打造的一款在线管理平台，它可以实时完成信息处理，还缩短社团信息信息管理流程，使其系统化和规范化。

系统功能说明

1、系统共有管理员、社团社长、学生三个角色，管理员拥有所有系统最高权限。

2、社团社长可以审核入团申请、发布社团活动、发起社团收费、发布并管理社团通知、管理社团成员

3、用户可以申请社团、查看系统、社团通知，查看并修改个人信息

4、.......

需改进之处

1、用户登录系统后、查看社团列表未对入团状态作出判断，用户可以重复申请入团，可限制用户单个社团只能加入一次，在社团列表页可做判断，如用户已加入该社团、不展示申请按钮。

2、社团活动列表页面，当社长将用户移除社团时，参与活动的人数并为改变，但实际只有一人，查看后台发现，人数是在活动表字段写死，并为实时统计。

3、创建社团只能管理员创建，可以考虑权限下放至用户。

4、管理员创建社团时需要指定社长，但是此处需要手动填写用户id，此处可考虑做成用户下拉列表。

5、4的问题在系统其他地方也存在，可考虑全部优化。

6、......

### 技术选型

开发工具：IntelliJ IDEA

运行环境：jdk8 + mysql5.7及以上 + maven + nodejs

服务端：SpringBoot + mybatis-plus

前端：Vue + axios + Element-UI + vuex

### 成果展示

用户注册

![image-20231109104324257](images/20231109104324.png)

用户登录

![image-20231109104339534](images/20231109104339.png)

主页面

![image-20231109103713044](images/20231109103720.png)

社团类型管理

![image-20231109103740122](images/20231109103740.png)

社团管理

![image-20231109103803069](images/20231109103803.png)

创建社团

![image-20231109103821508](images/20231109103821.png)

用户管理

![image-20231109103842855](images/20231109103842.png)

社团成员管理

![image-20231109103902807](images/20231109103902.png)

社团活动管理

![image-20231109103920029](images/20231109103920.png)

通知信息管理

![image-20231109103936374](images/20231109103936.png)

入团申请

![image-20231109104141683](images/20231109104141.png)

社团活动

![image-20231109104159644](images/20231109104159.png)

个人中心

![image-20231109104229236](images/20231109104229.png)

### 账号地址及其他说明

1.地址说明

登录页:http://localhost:8080/

2.账号说明

管理员：admin/admin

社长：user/123456

用户: student1/123456

3.运行说明

~~~sh
1、安装前置运行环境nodejs、maven，在client目录下打开cmd命令行，执行如下命令
npm install
2、依赖下载完之后，执行如下命令运行前端项目
npm run serve
3、自行配置maven环境，使用idea打开服务端项目，修改application.yml下的数据库配置
4、连接本地数据库，创建self_student_teams数据库，执行sql目录下的sql脚本
5、请使用5.7及以上版本的MySQL，如8.0以上版本，请自行修改驱动名称
~~~

### 源码获取

源码编号：003

源码加群看置顶群公告获取，QQ群：941095490